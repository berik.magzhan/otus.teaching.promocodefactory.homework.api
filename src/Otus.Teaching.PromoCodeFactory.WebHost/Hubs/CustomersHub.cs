﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Hubs
{
    public class CustomersHub : Hub
    {
        public async Task Send(string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", message);
        }
    }
}
