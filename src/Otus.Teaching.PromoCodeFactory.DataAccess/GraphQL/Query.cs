﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class Query
    {
        private readonly DataContext _dataContext;

        public Query(DataContext dataContext) => _dataContext = dataContext;

        public IQueryable<Customer> Customers => _dataContext.Customers;
        public IQueryable<Preference> Preferences => _dataContext.Preferences;
    }
}
